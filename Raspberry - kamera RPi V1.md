# Raspberry Pi - kamera V1

## 1) Test funkčnosti

- náhled na monitoru připojeném přes HDMI
  + -p 'x-pos,y-pos,width,height' 
  + -t s (čas v sekundách, 0=nepřetržitě)


```
raspistill -p '0,0,800,600' -t 0
```

- náhled na celou obrazovku (konec = `CTLR+X`)

```
raspistill -d
```
  
  

## 2) Připojení

```
sudo modprobe bcm2835 -v4l2
```

- zpřístupní kameru jako `/dev/video0`
- možno přidat do `/etc/rc.local` pro nastavování po rebootu
