# Raspberry Pi - autostart skriptů
# Metoda /etc/profile

### Nastavení Autologin pro uživatele pi
- provede se přes `raspi-config`, položka 3: Boot Options -> B1: Desktop/CLI -> B2: Console Autologin

### Nastavení spuštění skriptu
- v souboru sudo nano `/etc/profile` se na poslední řádek přidají požadované příkazy, např.:

```bash
cd /home/pi/python/TCU
python3 main.py &
```

- znak `&` znamená, že skript poběží na pozadí a systém zpřístupní příkazovou řádku

- nevýhodou je přístup skrz SSH, kdy se po přihlášení skript opět spustí