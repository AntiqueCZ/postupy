### Garáže - 4 žíly

\# | signál | barva
---|--------|-------
 1 |  Vcc   | white
 2 |  GND   | brown
 3 |  RSA   | yellow 
 4 |  RSB   | green
   

### BTL Timer

\# | signál | barva
---|--------|-------
 1 |  +12V  | Bílá
 2 |  LA1   | Zelená
 3 |  LA2   | Žlutá
 4 |  GND   | Hnědá
 5 |  TEMP  | Růžová
 6 |  GND   | Šedá


### motory - fáze
\# | fáze   | barva
---|--------|-------
 1 | PE     | žl/zel
 2 | L1     | černá
 3 | L2     | hnědá
 4 | L3     | šedá

### CAN bus @ 3-wire cable
(SAE J1939-11)

\# | signal   | barva
---|----------|-------
 1 | CAN high | white
 2 | CAN low  | green
 3 | GND      | hnědá
 