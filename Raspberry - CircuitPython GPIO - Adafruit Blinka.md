# Ovladače GPIO portů a sběrnic I2C a SPI Adafruit Blinka

- [CircuitPython on Linux and Raspberry Pi](https://learn.adafruit.com/circuitpython-on-raspberrypi-linux/overview)

- ovladače pracují pouze s Pythnon3

- Blinka je maskot CircuitPythonu (fialovo-růžovej had)


## Instalace potřebného SW

  ```bash
  # před instalací je dobré aktualizovat celý OS
  sudo apt-get update
  sudo apt-get upgrade
  
  sudo apt-get install python3-pip
  sudo pip3 install --upgrade setuptools
  ```

## Sběrnice I2C a SPI

- sběrnice I2C a SPI se povolují v konfiguračním nástroji

  ```bash
  sudo raspi-config

  # kontrola / zobrazení 
  ls /dev/i2c* /dev/spi*
  ```

## Python GPIO knihovny

  ```bash
  pip3 install RPI.GPIO
  
  pip3 install adafruit-blinka
  ```

## Test Blinka knihovny

 - create a new file called test_blinka.py with nano or your favorite text editor and put the following in

  ```python
  import board
  import digitalio
  import busio
  
  print("Hello blinka!")
  
  # Try to great a Digital input
  pin = digitalio.DigitalInOut(board.D4)
  print("Digital IO ok!")
  
  # Try to create an I2C device
  i2c = busio.I2C(board.SCL, board.SDA)
  print("I2C ok!")
  
  # Try to create an SPI device
  spi = busio.SPI(board.SCLK, board.MOSI, board.MISO)
  print("SPI ok!")
  
  print("done!")
  ```

 - save it and run at the command line with `python3 blinkatest.py`
 