# Nastavení WiFi na RPi3

- funguje na RPi3 i na RPi Zero-W

1) Na kartu s Raspbianem (Desktop či Lite) v rootu oddílu boot vytvořit soubor `wpa_supplicant.conf`

2) Do souboru vložit:

     ```
     ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
     update_config=1
     country=US

     network={
          ssid="AnDi24GHz"
          psk="Your WPA/WPA2 security key"
          key_mgmt=WPA-PSK
     }
     ```

3) Přepsat korektní údaje pro aktuální WiFi síť

4) Pro zapnutí SSH přístupu je nutno vytvořit v rootu na boot oddílu prázdný soubor ssh

5) SD kartu vložit do RPi a zapnout napájení  

&nbsp;

&nbsp;

# Odpojení Wifi na Raspberry Pi3

- např. kvůli použití USB Wi-Fi dongle s vyšším dosahem

- v rootu boot oddílu v souboru `config.txt` přidat řádky:

```bash
# Disables onboard Wi-Fi and Bluetooth
dtoverlay=pi3-disable-bt
dtoverlay=pi3-disable-wifi
```
&nbsp;

&nbsp;

# Identifikace USB Wi-Fi Dongle

```bash
# výpis USB připojených zařízení
lsusb

# if your device description says "Unknown device", you can update your local usb-id definition by running update-usbids as root
sudo update-usbids

# to get something slightly more verbose, but still readable, use:
lsusb -v | grep -E '\<(Bus|iProduct|bDeviceClass|bDeviceProtocol)' 2>/dev/null

# výpisl vlastností Wi-Fi modulu
iw list
```
