﻿# Rapsberry - Nastavení WiFi bez připojení klávesnice a monitoru

[Comitup](https://davesteele.github.io/comitup/)

- projekt Comitup umožňuje připojit RPi k WiFi síti bez připojené klávesnice a monitoru
    - po zapnutí se spustí RPi v režimu AP
    - po připojení k takto vytvořené síti telefonem s Androidem (možná i z NTB s Win10) se automaticky přepne na www stránky, kde se nastaví připojení k místní síti WiFi (umožní zvolit jméno z vyhledaných sítí a zadat heslo), jinak je IP adresa: 10.41.0.1
    - po dohledání IP adresy se lze k RPi připojit přes SSH