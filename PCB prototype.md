# Podmínky pro výroby prototypových DPS

## 1) [Itead](www.itead.cc)

- cena 9.90$ za 10ks DPS o max. rozměrech 5×5cm

### Podmínky výroby  

Parameter                   | Value
----------------------------|-------------------------
Layers                      | 1 - 4
Material                    | FR-4
Board Dimension (max)       | 380mm X380mm
Board Dimension (min)       | 10mm X10mm
Conductor Width (min)       | 0.15mm( Recommend>8mil )
Conductor Space (min)       | 0.15mm( Recommen>8mil  )
Copper to Edge              | >0.3mm
Plated via Diameter (mech.) | 0.3mm--6.30mm
Hole Space(min)             | 0.25mm
Hole to Edge                | 0.4mm
Annular Ring(min)           | 0.15mm
Solder Resist Clearance     | 0.1mm
Silkscreen line width (mim) | 6mil (0.15mm)



  
   
### Pojmenování souborů  

Layer:                     |  Filename:
---------------------------|----------
Top layer:                 |  pcbname.GTL                      
Inner Layer 1:             |  pcbname.GL1 (for 4-layer board)    
Inner Layer 2:             |  pcbname.GL2 (for 4-layer board)    
Bottom layer:              |  pcbname.GBL                      
Solder Stop Mask top:      |  pcbname.GTS                     
Solder Stop Mask Bottom:   |  pcbname.GBS                         
Silk Top:                  |  pcbname.GTO                        
Silk Bottom:               |  pcbname.GBO                         
NC Drill:                  |  pcbname.TXT                              
Outline layer:             |  pcbname.GKO                       


Note:
All the gerber files must be in RS-274x format, except the drill file should be in Excellon format. 
If there is no pcbname.GKO or DO file, please make sure the PCB outline or shape shall at least be specified in other layer, silkscreen or soldermask layer etc. 
If soldermask layers are not required, please clarify surface finish for both sides when you submit your gerber files. 



## 2) [PCBway](www.pcbway.com)


