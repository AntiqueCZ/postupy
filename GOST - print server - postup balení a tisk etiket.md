# Etikety na balení - manuál

## 1) Spuštění programu

- 1-1)   Spustit PC „Print server“, připojit se Remote Administratorem v3.4.

- 1-2)   Spustit program SN Server. 

![SN Server](img/GOST_-_print_server_-_postup_balení_a_tisk_etiket/image001.png)

- 1-3)   Spustit program SN Client, zadat user: `sup` pass: `sup`

![SN Server](img/GOST_-_print_server_-_postup_balení_a_tisk_etiket/image002.png) ![SN Server](img/GOST_-_print_server_-_postup_balení_a_tisk_etiket/image003.png)

- 1-4)   Konfigurace SN Clienta je na obr.1:

![SN Server](img/GOST_-_print_server_-_postup_balení_a_tisk_etiket/image004.png)


## 2) Tisk etiket
- 2-1) Stisknutím tlačítka `[Balení výrobků]` otevřít dialogové okno:

![SN Server](img/GOST_-_print_server_-_postup_balení_a_tisk_etiket/image005.png)

- 2-2) Zde vybrat kod výrobku. Nový výrobek a jeho parametry balení se zadávají do databáze v jiné části programu.

- 2-3) Do pole "*Kod zakázky interní*" zadat číslo příslušného výrobního příkazu.

- 2-4) Z roletky vybrat firmware nahraný do výrobku. Tlačítkem `[Vložit]` jej přidat do seznamu firmwarů.

- 2-5) Tlačítkem `[Balit]` otevřít dialogové okno Balení.

![SN Server](img/GOST_-_print_server_-_postup_balení_a_tisk_etiket/image006.png)

- 2-6) Do pole Výr. číslo zapsat sériové číslo výrobku a tlačítkem `[Vložit]` jej zapsat do databáze. Není-li zaškrtnuto "*Automatický tisk*", vytisknout etiketu tlačítkem `[Tisk etikety]`.

- 2-7) Při zaškrtnuté autmomatice se po vložení sériového čísla etiketa vytiskne, balení se uzavře a přesune do vyšší úrovně, kde se po naplnění balení (dle zvoleného balícího schématu) též uzavře a vytiskne další etiketa.




## 3) Zadávání položek do databáze

### 3.2) Firmware

- 3.2-1) V dialogovém okně "Kmenová data" vybrat záložku Firmware.

![SN Server](img/GOST_-_print_server_-_postup_balení_a_tisk_etiket/image007.png)

- 3.2-2) Stisknout tlačítko [Nový záznam].

- 3.2-3) Do pole Verze zapsat verzi (jméno binárního souboru) vkládaného firmware.

- 3.2-4) Do pole Klíč zařízení vybrat kod výrobku (pouze prvních šest cifer), pro který je vkládaný firmware určen.

- 3.2-5) Do pole Obvod vybrat příslušný obvor pro který je vkládaný firmware určen.

- 3.2-6) Tlačítkem [Uložit] nové údaje vložit do databáze.

 