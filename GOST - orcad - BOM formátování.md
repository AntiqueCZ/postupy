### OrCAD v9.x BOM

- formátování exportu BOM: do části `Line Item Definition` vložit 

> Header

```
Item\tQty\tReference\tPart\tPart Field
```


> Combined property string


```
\n{Item}\t{Quantity}\t{Reference}\t{Value}\t{1st Part Field}
```
