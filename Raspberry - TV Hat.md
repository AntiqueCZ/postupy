
# Instalace TV Hat DVB-T tuneru
- instalace driverů pro originální RPi tuner není třeba, jsou již v jádře Raspbianu

<br />&nbsp;
## Aktualizace čistého systému
- operace na cca 20 minut


```bash
sudo apt update
sudo apt upgrade
sudo reboot
```

<br />&nbsp;
## Příkazy na kontrolu instalace driverů TV tuneru

```bash
lsusb    # výpis usb zařízení
lsmod    # výpis naštených modulů (driverů)
```

# Instalace SW Tvheadend
- operace na dalších cca 20 minut

```bash
sudo apt install tvheadend
```

- nastavení parametrů tvheadend ve webovém rozhraní na adrese: `http://<rpiaddres>:9981`