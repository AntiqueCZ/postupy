# OrCAD 386+ Instalace na PC


OrCAD se spouští ve virtuálním PC, např. free projektu DOSbox v0.74.
Nejdůležitější parametry nastavení v souboru `dosbox.conf`:

```dos
[dosbox]
language=
machine=svga\_et4000
captures=capture
memsize=63

[cpu]
core=auto
cputype=auto
cycles=50000
cycleup=500
cycledown=20

[autoexec]
mount c: c:\

#adresář s instalací orcadu a soubory projektů
mount g: c:\DOS
#mount o: o:\

set dos16m=:12M
set copycmd=/y
SET PATH=D:\C251\BIN;C:\DOS;D:\UTIL;D:\BC\BIN;D:\C32\BIN;C:\ztree;D:\C51P;G:\ORCADEXE;c:\win2k

SET ORCADEXE=G:\ORCADEXE\
SET ORCADPROJ=G:\ORCAD\
SET ORCADUSER=G:\ORCADESP\
SET EDPATH=F:\WAT\EDDAT
SET INCLUDE=F:\nlm\novh;F:\wat\h;.;e:\ot52\include
SET WATCOM=F:\WAT
SET C251LIB=D:\C251\LIB
SET C251INC=D:\C251\INC

del c:\swapfile.tmp
orcad
exit
```
