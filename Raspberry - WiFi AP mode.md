# Spuštění a nastavení WiFi Acces Pointu

- [Setting up a Raspberry Pi as a Wireless Access Point](https://www.raspberrypi.org/documentation/configuration/wireless/access-point.md)


## Instalace potřebného SW

  ```bash
  # před instalací je dobré aktualizovat celý OS
  sudo apt update
  sudo apt upgrade

  sudo apt install dnsmasq hostapd
  
  # kvůli úpravě konfiguračních souborů je nutné je hned po instalaci vypnout
  sudo systemctl stop dnsmasq
  sudo systemctl stop hostapd
  ```

  #### Konfigurace statické adresy RPi
   - v souboru `/etc/dhcpcd.conf` na konec přidat řádky:

   ```bash
   interface wlan0
       static ip_address=192.168.4.1/24
       nohook wpa_supplicant
   ```   

   - a restartovat daemona:
    
   ```bash
   sudo service dhcpcd restart
   ```

  #### Konfigurace DHCP serveru
   - služba dnsmasq
   - normálně má defaultní konfigurace plno zbytečných nesmyslů a nebude využita

  ```bash
  # záloha původního souboru
  sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig

  # editace nového souboru
  sudo nano /etc/dnsmasq.conf
  ```
  
  ```
  interface=wlan0      # Use the require wireless interface - usually wlan0
  dhcp-range=192.168.4.2,192.168.4.20,255.255.255.0,24h
  ```

 - a restartovat službu (možná bude odpověď: "dnsmasq.service is not active, cannot reload.", ale to nevadí, netřeba se znepokojovati)


  #### Konfigurace AP
   - služba hostapd
   - do souboru `/etc/hostapd/hostapd.conf` vložit následující řádky s konfigurací

  ```
  interface=wlan0
  driver=nl80211
  ssid=RPi-AnDi
  hw_mode=g
  channel=7
  wmm_enabled=0
  macaddr_acl=0
  auth_algs=1
  ignore_broadcast_ssid=0
  wpa=2
  wpa_passphrase=asdfghjkl
  wpa_key_mgmt=WPA-PSK
  wpa_pairwise=TKIP
  rsn_pairwise=CCMP
  ``` 

   - systému je třeba ukázat tuto konfiguraci, v souboru `/etc/default/hostapd` najít řádek s `#DAEMON_CONF` a přepsat jej na:

  ``` 
   DAEMON_CONF="/etc/hostapd/hostapd.conf"
  ``` 

  - už jen nastartovat službu:

  ```bash
  sudo systemctl unmask hostapd
  sudo systemctl enable hostapd
  sudo systemctl start hostapd

  # kontrola stavu
  sudo systemctl status hostapd
  sudo systemctl status dnsmasq
  ```

  - Add routing and masquerade:
  - v souboru `/etc/sysctl.conf` pozměnit a odkomentovat řádek:
  


  ```bash
  # add a masquerade for outbound traffic on eth0:
  sudo iptables -t nat -A  POSTROUTING -o eth0 -j MASQUERADE
  
  # save the iptables rule.
  sudo sh -c "iptables-save > /etc/iptables.ipv4.nat"
  ``` 

  - v souboru `/etc/rc.local` vložit NAD řádek `exit 0` 
  
  ```
  iptables-restore < /etc/iptables.ipv4.nat
  ``` 
  
  - reboot RPi a poté ověřit funkčnost, ve vzduchu by již měla být vidět nová WiFi síť
 
