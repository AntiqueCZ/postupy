﻿# Senzor vlhkosti a teploty AM2302/DHT22

- [DHT Humidity Sensing on Raspberry Pi with GDocs Logging](https://learn.adafruit.com/dht-humidity-sensing-on-raspberry-pi-with-gdocs-logging/python-setup)
- použití knihovny Adafruit Blinka


## Zapojení senzorů

pin1 - Vcc  
pin2 - Data  
pin3 - N.C.  
pin4 - GND  

- datový pin musí být připojen pull-up resistorem (10k) na Vcc

## Instalce knihoven

- musí být již nainstalován základ [Adafruit Blinka](Raspberry%20-%20CircuitPython%20GPIO%20-%20Adafruit%20Blinka.md)
- dále pak konkrétní knihovny pro senzor

```bash
pip3 install adafruit-circuitpython-dht
sudo apt-get install libgpiod2
```

## Test senzorů

- to make sure you've installed everything correctly, we're going to test that we can read values from the DHT sensor connected to your device

- create a new file called `sudo nano dht_simpletest.py` with nano or your favorite text editor and put the following in:

```python
import time
import board
import adafruit_dht

# Initial the dht device, with data pin connected to:
# DHT22 je i pro AM2302
# D18 je GPIO číslo pinu

dhtDevice = adafruit_dht.DHT22(board.D18)

while True:
    try:
        # Print the values to the serial port
        temperature_c = dhtDevice.temperature
        temperature_f = temperature_c * (9 / 5) + 32
        humidity = dhtDevice.humidity
        print("Temp: {:.1f} F / {:.1f} C    Humidity: {}% "
              .format(temperature_f, temperature_c, humidity))

    except RuntimeError as error:
        # Errors happen fairly often, DHT's are hard to read, just keep going
        print(error.args[0])

    time.sleep(2.0)
```

- then, save the example. Next, run the example by typing the following command into the terminal: `python3 dht_simpletest.py`




*******