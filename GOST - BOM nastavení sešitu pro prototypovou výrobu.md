
## Nastavení Excelovského sešitu pro prototypovou výrobu

### 1) Ukotvení prvního řádku

\\Zobrazení\Okno\Ukotvit příčky

### 2) Položky prvního řádku

Qty | Vych. | Designator | VALUE | Footprint | Description | HC | TME | DigiKey

### 3) Formátování zýrazňovačů

\\Domů\Styly\Podmíněné formátování\Správa pravidel -> Zobrazit pravidla pro tento sešit

vytvořit `Nové pravidlo` a `Určit buňky k formátování pomocí vzorce` 

do pole `Formátovat hodnoty, pro které platí tento vzorec` zadat vzorec `=$G2>0` (buňka G2 je neprázdná, resp. cokoli je větší než nula, i řetězec)

Formát vybrat zelenou / zelenkavou výplň a potvrdit `OK`

V řádku s nastaveným pravidlem vybrat oblast sešitu, kde toto pravidlo platí (vybrat oblast všech buněk kde jsou data)

Seznam pravidel:  
1- `=$G2>0` zelená, materiál má HC electronic  
2- `=$B2>0` žlutá, materiál je vychystán a připraven k závozu na osazení  
3- `=$I2>0` červená, materiál je nutné objednat v DigiKey/Mouser/...  
4- `=$H2>0` modrá, materiál objednán v TME  

### 4) Oblast tisku

Označit sloupce `A` až `G` a vybrat:

\\Rozložení stránky\Vzhled stránky\Oblast tisku\Nastavit oblast tisku

při tisku nastavit parametr `Přizpůsobit všechny sloupce na jednu stránku`
