# Instalace nového systému na PC

## 1) Windows 10 Pro

- instalaci lze spustit ze "starého" systému
- klíč již není potřeba, aktivace jde ze serverů Microsoftů

### Quick Launch

```bash
%UserProfile%\AppData\Roaming\Microsoft\Internet Explorer\Quick Launch
```

## 2) Základní software

- [Opera Browser](https://www.opera.com/cs/download)
  - [Install Chrome Extensions](https://addons.opera.com/cs/extensions/details/install-chrome-extensions/)
  - [Markdown Preview Plus](https://chrome.google.com/webstore/detail/markdown-preview-plus/febilkbfcbhebfnokafefeacimjdckgl)
  - [imTranslator](https://addons.opera.com/cs/extensions/details/imtranslator-translator-dictionary-tts/)
  - [Tipli do prohlížeče](https://chrome.google.com/webstore/detail/tipli-do-prohlížeče/dbnfnbehhjknomdbfhcobpgpphnlnikp)
  
- [Total Commander](https://www.ghisler.com/download.htm)
  - a: `\\Gost-sbs01\SBS01-D - USER DATA\KUNCL`
  - b: `\\Nas-06\zalohy\KUNCL`
  - m: `\\Nas-06\Public`
  - t: `\\Nas-06\Public\ZZ-K Založení 3\torrents`
  - v: `\\Gost-sbs01\SBS01-E - Výroba`
  
- WinRAR

- [MS Visual Code](https://code.visualstudio.com/download)

- Adobe Acrobat Standard

- Microsoft Office 2010

- [PSPad](http://www.pspad.com/cz/download.php)

- Zoner

- Corel

- Oracle Virtual Box

- [FreeFileSync](https://freefilesync.org)


## 3) SAP

- musí být nainstalován .NET Framework 3.5 (který obsahuje i 2.0 a 3.0)
  - instalace přes \\Start Menu\Aplikace a Funkce\Programy a funkce\Zapnout nebo vypnout funkce Windows -> .NET Framework 3.5
  - v případě chyby instalace 0x800F0954 je nutné povolit stahování funkcí ze serverů MS:
    - Win+R -> gpedit.msc -> Konfigurace počítače -> Šablony pro správu -> Systém
    - položku `Zadat nastavení pro instalaci volitelných součástí a opravu součástí` nastavit na `Povoleno` a zaškrtnout volbu `Stáhnout obsah ... ze služby ... (WSUS)`
    - potvrdit, restartovat a znovu spustit instalaci
- 

## 4) H-Výroba

## 5) Elektro & Programování

- [GIT](https://git-scm.com/downloads)

- DOS box

- J-Flash v6.10g

- SeCall
  - v3.5
  - v4.14
  - v5.x

- ManageIP.exe
  
- Hyper Terminal

- Atmel Studio v7

- Altium v19

- KiCAD

## 6) Multimédia

- [VLC player](https://www.videolan.org/vlc/index.cs.html)

- AIMP přehrávač MP3

## 7) Komunikace

- Skype

- [WhatsApp](https://www.whatsapp.com/download)
  - [WhatsAppTray](https://github.com/D4koon/WhatsappTray/releases)

- Team Viewer 14
