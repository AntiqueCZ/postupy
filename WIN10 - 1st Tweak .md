# Win 10 - 1st Tweak

## Dialogová okna nastavení systému Win10

| Dialog             | Příkaz
---------------------| ---------------------
| Vlastnosti systému | sysdm.cpl
| Síťová připojení   | ncpa.cpl


## The Quick Launch
The Quick Launch folder is located at the hidden path below.

```dos
%UserProfile%\AppData\Roaming\Microsoft\Internet Explorer\Quick Launch
```

&nbsp;<br/>

## Přehled programů pro základní instalaci

1) [Chrome](https://www.google.com/chrome/)  
2) [Total CMD](https://www.ghisler.com/download.htm)  
3) [T-Clock](https://github.com/White-Tiger/T-Clock/releases)  
4) [PSpad](http://www.pspad.com/cz/)  
5) [VLC](https://www.videolan.org/vlc/index.cs.html)  


&nbsp;<br/>

## Přehled programů pro podporu programování
1) [GIT]()  
2) [Markdown Plus](https://raw.githubusercontent.com/tylingsoft/dist/master/mdp-2.3.0.exe)  
    - vyžaduje .NET Framework 4.5.2 nebo vyšší - součást Win10  
    - vyžaduje [MS Visual 2013 Redistributable](https://download.microsoft.com/download/2/E/6/2E61CFA4-993B-4DD4-91DA-3737CD5CD6E3/vcredist_x86.exe)  

&nbsp;<br/>

## Přehled programů pro multimédia
1) [meGUI](https://sourceforge.net/projects/megui/)  
2) [youtube-dl](https://youtube-dl.org/)  
3) [FreeRapid Downloader](http://wordrider.net/freerapid/)  
4) [MediaInfo](https://mediaarea.net/en/MediaInfo)  
5) [MKVToolnix](https://www.fosshub.com/MKVToolNix.html)  
    - vyžaduje [MS Media Foundation Framework](https://www.microsoft.com/cs-CZ/download/confirmation.aspx?id=48231) , který není v edici Win10N

&nbsp;<br/>

## Ostatní programy
1) [FreeFileSync](https://freefilesync.org/download.php)  
2) [Kerio](http://download.kerio.com/archive/) (pouze verze 9.1.0!!!)

&nbsp;<br/>

## Vypnutí Řízení uživatelských účtů

1) po stisku tlačítka StartWin dát hledat řetězec 'UAC'  
2) spustit 'Změnit nastavení nástroje Řízení uživatelských účtů'  
3) nastavit šoupák na minimum  

&nbsp;<br/>

## Aktivace MS Office 2010

Aktivace je zatím stále možná na telefonu do Microsoftu +420 225 990 844 

&nbsp;<br/>

## Nastavení T-Clock

V programu T-Clock\Option\Time Format zaškrtnout Advanced time format a zadat hodnotu:

`| dddd | dd.mm.yy | HH:nn |`

V záložce Clock Text nastavit font na Consolas, Bold, 13pt

&nbsp;<br/>


## Test výkonu PC

1) V příkazové řádce (s právy admin) spustit příkaz pro testování výkonu (výsledek je uložen do XML souborů):
```dos
winsat prepop
```

2) V PowerShellu (s právy admin) zobrazit výsledky měření:
```shell
Get-WmiObject -Class Win32_WinSAT
```

- **CPUScore** is the score for the processors on the PC. 
- **D3DScore** is the score for the 3D graphics capabilities of the PC.
- **DiskScore** is the score for the sequential read throughput on the system hard disk. 
- **GraphicsScore** is the score for the graphics capabilities of the PC.
- **MemoryScore** is the score for the memory throughput and capacity of the PC.