
# OrCAD386+ Pomocná gerber vrstva - overlay

&nbsp;</br>
-----------
## 1. Data
-----------

- získaná projektová data uložit do c:\\DOS\\ORCAD (**zachovat originál v zipu!!!**)
- případné nové knihovny uložit do c:\\DOS\\ORCADESP\\SDT\\LIBRARY

Projekt s požadovanými daty souřadnic se otevře v sekci „Design Management Tools" (odklepnutím Execute). V levé části (Design) jsou adresáře s projekty, v pravé již konkrétní soubory se schématy \*.sch. Výběrem konkrétního souboru a potvrzením `[OK]` se schéma otevře a program se vrátí do úvodního menu se sekcemi.





&nbsp;</br>
----------------------
## 2. Úprava netlistu
----------------------

- Schematic Design Tool  
    - Create Net List > Configure Schematic Tools

- až dole zadat do `Part Value Combine` hodnotu `R` (_R=reference, V=value_)

- potvrdit `OK` a `Execute` (vytvoří se upravený netlist)

- odejít zpět do hlavní nabídky To Main




&nbsp;</br>
----------------------------------
## 3. Nastavení parametrů Layoutu  
----------------------------------

- PC Board Layout Tools

- Edit Layout
 
- sekvencí klávesových zkratek GEBB otevřít nastavení  
    **G**oto - n**E**tlist loader - **B**lock - **B**lockend

- vybrat upravený netlist + `OK`
- odškrtnout všechny tlačítka/přepínače
- potvrdit `OK`

- klávesovou zkratkou **S**et otevřít nastavení a nastavit:
   - [x] Hide reference designator text
   - [ ] Hide module value text

- potvrdit `OK`, na DPS musí být reference již uprostřed SMD

     > V případě, že se zobrazí pouze hodnoty místo referencí je nutno:  
     > Create Netlist > Local Configuration > Configure INET  
     > zaškrtnout "Unconditionally process all sheets in design"



&nbsp;</br>
-------------------------------------
## 4. Vygenerování grb vrstvy layout  
-------------------------------------

- sekvencí klávesových zkratek GT otevřít nastavení printout  
    **G**oto - prin**T**

- tlačítkem `[Load]` načíst přednastavenou konfiguraci \*.PFG
- tlačítkem `[Begin All]` spustit generování grb všech vrstev

- sekvencí klávesových zkratek QA opustit návrhář DPS  
    **Q**uit - **A**bandon program

![OrCAD](img/GOST_-_OrCAD386+_pomocná_gerber_vrstva/image1.png)

![OrCAD](img/GOST_-_OrCAD386+_pomocná_gerber_vrstva/image2.png)

![OrCAD](img/GOST_-_OrCAD386+_pomocná_gerber_vrstva/image3.png)

![OrCAD](img/GOST_-_OrCAD386+_pomocná_gerber_vrstva/image4.png)




prosím o radu, jak přesvědčit orcad v generování grb silkbottom, aby mi ji vyexportoval zrcadleně?
tlačítko Mirrored je zaškrtlé, ale nějak na něj dlabe...
Jde mi o tu vrstvu s posunutými referencemi na střed...

hornicek_pavel, 16:11Advanced, tam je pak mirror X. Ten mirror tady je jenom o zrcadleni textu.A bacha, uchylna logika je takova, ze to neumi update, ale musis predchozi smaznout a znova pridat.16:18to jsem potřeboval vědět... už to mám tyak jak potřebuji...