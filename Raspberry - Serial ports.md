
# Sériové porty na Raspberry Pi

- napěťové úrovně signálů Tx a Rx jsou 0 ÷ 3.3V
- RPi má dva UARTy, jeden plný vč. HW signálů řízení a jeden miniUART
- Bluetooth je na RPi sdílen přes plný UART, proto chci-li plný UART využít, musím vypnout Bluetooth



<br />&nbsp;
## Sériové porty v Linuxu

- Standardní typy sériových portů v Linuxu: 
   - the Linux standard is `/dev/ttyS0`, `/dev/ttyS1`, and so on
   - USB Serial Port Adapter: `/dev/ttyUSB0`, `/dev/ttyUSB1`, and so on.
   - Some types of USB serial adapter may appear as `/dev/ttyACM0` ...

- výpis zařízení sériových portů 

```bash
ls -l /dev/tty*
```

<br />&nbsp;
## Raspbian Lite

- Raspberry Pi Zero W má sériový port na `ttyS0`
- na konektoru jsou to GPIO14 a GPIO15 (pin 8 a pin 10)


### Nastavení portu
1) povolit sériový port 
   ```bash
   sudo raspi-config
   ```


2) zakázat výpis debug informací raspbianu do uart: 
   -  the main menu, select option 7 (Serial), then select 'No' to disable shell and kernel messages via UART

3) v souboru `sudo nano /boot/config.txt` přidat na konec řádek: `enable_uart=1`
   ```bash
   sudo nano /boot/config.txt
   ```




<br />&nbsp;
## Terminál / konzole sériového portu Minicom

### Instalace

```bash
sudo apt-get install minicom
```

### Spuštění konzole

```bash
minicom -b 115200 -o -D /dev/ttyS0
```

