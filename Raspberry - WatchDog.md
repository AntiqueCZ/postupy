# Raspberry Pi - WatchDog
# metoda crontab

WDT je časovač, který se musí po určité době nulovat (neustále se zasílá příkaz pro vynulování). V případě zamrznutí systému se přestane WDT nulovat a tím dojde k přetečení časovače WDT. WDT provede restart systému. Tzn. v případě problému (zakousnutí) vyřeší WDT reset RPI.

Broadcom BCM2708 obsahuje i HW Watchdog. Máme k dispozici dva způsoby použití watchdogu v RPi (a nebo b, ale nelze obojí!)

## 1) WatchDog Daemon

```bash
# instalace...
sudo apt-get install watchdog

# a konfigurace...
sudo nano /etc/watchdog.conf
```
- odkomentovat na řádcích (nebo i jiné křížky):

```bash
watchdog-device = /dev/watchdog
max-load-1             = 24
```

- v souboru /etc/modules přidat položku:

```bash
sudo nano /etc/modules

# watchdog
bcm2708_wdog
```


- spustit příkaz k aktivaci modulu a služby:

```bash
sudo modprobe bcm2708_wdog 
sudo service watchdog restart
sudo apt-get install watchdog chkconfig
sudo chkconfig watchdog on
sudo /etc/init.d/watchdog start
```


- pro test služby zahltit systém (RPi se během chvíle samo restartuje):

```bash
:(){ :|:&};:
```

## 2) software WatchDog 

- hlídá běh pythonovského programu

```bash
# nejprve instalace
sudo apt-get install python-dev
sudo apt-get install python-pip
sudo pip install watchdogdev
```

- v souboru /etc/modules přidat položku:

```bash
sudo nano /etc/modules

# watchdog
bcm2708_wdog
```

- spustit aktivaci modulu a služby:

```bash
sudo modprobe bcm2708_wdog
```

- příklad použití:

```python
#!/usr/bin/env python
import subprocess
from time import sleep
from watchdogdev import *

wd = watchdog('/dev/watchdog')

# povolime hw watchdog
command = 'sudo modprobe bcm2708_wdog'
subprocess.Popen(command , shell=True,stdout=subprocess.PIPE)

print "Watchdog bezi a hlida chod programu"

# smycka
while True:
      wd.keep_alive() # musime do 10 sec vzdy spustit jinak restart rpi
      print "WDT cas= ",wd.get_time_left()
      sleep(1)

# pokud program ukoncime dojde do 10 sekund k restartu RPI

# pro ukonceni wdt je nutne do naseho programu pridat:
# wd.magic_close() # vypne wdt a nedojde k restartu RPI
```