
-----------------------------------------
## Převod BOM z Bublinovo schémat na SAP
-----------------------------------------
</br>

Bublina       | SAP                                     | kód
------------- | --------------------------------------- | -------------------
SM5400        | 1N4007                                  | 121-0103-001
D SMD         | 1N4148                                  | 121-0101-001
SEMIC x       | PAW2103                                 | 225-0501-2103-0x
SMAJ10A       | SMAJ12CA                                | 121-0301-B012
RSD42V        | Filtr 0,5A Tronic                       | 226-0101-001
TL150mic      | 150uH 1,5A+100mA toroid TRONIC          | 226-0201-003
MS628512Z     | BS62LV1024SC-70                         | 152-0401-001
AM29F040      | AT29F040-90JC                           | 152-0101-002
ETHTRAFO      | 20F001N YCL trafo 10 base T - RTL8019AS | 255-0302-001
TRANSIL       | P6KE12CA DO15                           | 221-0301-001
IRLML2502     | IRLML2030 SOT23  MOSFET                 | 122-0202-ILML2030
RF45FS        | RJ45 8/8 miniaturní                     | 225-0107-003
RJ45F         | RJ45 8/8 kolmý A-2014-2 ASSMANN         | 225-0107-002
BEAD-4        | 80R@100MHz 200mA                        | 126-0401-7427927280
BEAD-6        | 100R@100MHz 500mA                       | 126-0401-742792620
MICROSD KLAP  | micro SD card conector odklápěcí        | 125-0303-101
USB_CON_90    | USB B na DPS 180st                      | 225-0110-002
CHLADIC SEMIC | F16-2-220 TO220 chladič se zarážkou     | 290-0101-003
FB-8          | WE 742 792 040                          | 126-0301-742792040