# Raspberry Pi - autostart skriptů: init.d
- nepodařilo se rozchodit!!!

## 1) Spouštěcí script

- v adresáři `/etc/init.d` vytvořit spustitelný bash script:

```bash
# neexistující sobor se případně vytvoří...
sudo nano /etc/init.d/muj-autostart
```

- soubor musí mít tuto následující strukturu:
    + `esac` je ukončení příkazu/bloku case - esac
    + `fi` je ukončení příkazu/bloku if - fi
    + `;;` netuším

- o spouštění linuxu se stará program init (v novém linuxu je to systemd, který je ale velmi kritizován a proto se funkčnost init stále zachovává)

- init spouští programy po startu počítače podle režimu, ve kterém se počítač má nacházet po startu (tzv. Runlevel). Platné runlevely jsou:
    + 0: HALT - systém je připraven na ukončení
    + 1: SINGLEUSER - jednouživatelský systém bez sítě, slouží pro opravy po havárii
    + 2-5: MULTIUSER - normální boot
    + 6: RESTART - systém se restartuje

- nejdůležitější runlevely, se kterými se u raspberry setkáme jsou 0, 5 a 6. Aktuální runlevel na kterém systém běží zjistíme příkazem:

```bash
$ runlevel
N 5
```
- z výpisu je vidět že právě běží runlevel 5 a do něj se systém dostal přechodem z čistého bootu (N).

- všechny programy spouštěné po startu unixu jsou spouštěny tzv. spouštěcím skriptem. Tento skript musí reagovat na parametr příkazového řádku start a stop. Tento skript musí být instalován v /etc/init.d a musí mít na začátku následující konfigurační text:

- tento text říká programu init, pro které runlevely se má skript spustit, pro které ukončit a v jakém pořadí má být vůči ostatním spouštěcím skriptům. Parametr $all na konci znamená že se skript spustí jako poslední


```bash
#!/bin/sh
# /etc/init.d/muj-autostart

### BEGIN INIT INFO
# Provides:          <scriptname>
# Required-Start:    $remote_fs $syslog $all
# Required-Stop:     $remote_fs $syslog $all
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start daemon at boot time
# Description:       Enable service provided by daemon.
### END INIT INFO

case "$1" in
  start)
    # zde je místo pro spuštění nějakého výkoného skriptu/aplikace
    # tato větev se spustí při startu linux systému
    echo "Start nějaké aplikace"
    ;;
  stop)
    # zde je místo pro spuštění nějakého výkoného skriptu/aplikace
    # tato větev se spustí při vypínání linux systému
    echo "Ukončovací skript"
    ;;
  *)
    echo "Usage: /etc/init.d/muj-autostart {start|stop}"
    exit 1
    ;;
esac
exit 0
```
  
  

## 2) Nastavení parametrů skriptů

- souboru `/etc/init.d/muj-autostart` je nutno nastavi příznak spustitelnosti

```bash
sudo chmod 755 /etc/init.d/muj-autostart
```

- a jakési nastavení vlastní spustitelnosti (registrace?), pouhá existence souboru nestačí

```bash
sudo update-rc.d muj-autostart defaults
```
