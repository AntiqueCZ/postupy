# Thonny - Nastavení barevného schématu



Najít soubor `/usr/lib/python3/dist-packages/thonny/plugins/base_syntax_themes.py` a povolit mu práva ke změně obsahu pro kohokoli. Poté do něj vložit definici nového schématu:

```python
def antiquecz() -> SyntaxThemeSettings:
    normal_fg = "#e7e7e7"
    string_fg = "#e48864"

    return {
        "TEXT": {
            "foreground": normal_fg,
            "insertbackground": normal_fg,
            "background": "#252525",
        },
        "GUTTER": {"foreground": "#7f8f8f", "background": "#464646"},
        "current_line": {"background": "#4A4A4A"},
        "sel": {"foreground": "white", "background": "#506070"},
        "number": {"foreground": "#8cd0d3"},
        "definition": {"foreground": "#f4a020", "font": "BoldItalicEditorFont"},
        "string": {"foreground": string_fg},
        "string3": {"foreground": string_fg},
        "open_string": {"foreground": string_fg},
        "open_string3": {"foreground": string_fg},
        "keyword": {"foreground": "#da72da", "font": "BoldEditorFont"},
        "builtin": {"foreground": "#efef8f", "font": "BoldEditorFont"},
        "comment": {"foreground": "#62ac42", "font": "ItalicEditorFont"},
        "prompt": {"foreground": "#87ceeb"},
        "stdin": {"foreground": normal_fg},
        "stdout": {"foreground": "#eeeeee"},
        "value": {"foreground": "#eeeeee"},
        "stderr": {"foreground": "#ff0000"},
        # paren matcher
        "surrounding_parens": {"foreground": "white", "font": "BoldEditorFont"},
    }
```  

Pro načtení do programu vložit řádek:

```python
get_workbench().add_syntax_theme("AntiqueCZ", "Default Dark", antiquecz)
```

do deklarace definice `load_plugin() -> None` na konci tohoto souboru. 

Pro uplatnění schématu je nutno restartovat program.