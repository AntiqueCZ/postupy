## Dialogová okna nastavení systému Win10

| Dialog                     | Příkaz
---------------------        | ---------------------
| Vlastnosti systému         | sysdm.cpl
| Programy a funkce          | appwiz.cpl
| Správce autorizací         | azman.msc
| Pokročilé uživatelské účty | netplwiz
| Síťová připojení           | ncpa.cpl
| Brána firewall             | firewall.cpl