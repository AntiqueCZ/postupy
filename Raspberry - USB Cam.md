# Raspberry Pi - USB Camera

Instalace USB kamery na RPi4

- [Generalplus MD80 Mini Spy Cam (type 808)](http://nikolaynotebook.blogspot.com/2016/02/raspberry-pi.html)



## 1) Prep & Optional Tools

 ```bash
 # některé položku mohou být již v systému nainstalované
 sudo apt-get update
 sudo apt-get upgrade

 sudo apt-get install guvcview
 sudo apt-get install uvcdynctrl
 sudo apt-get install uvccapture
 sudo apt-get install qv4l2
 sudo apt-get install mplayer
 sudo apt-get install v4l-utils

 sudo reboot
 ```


## 2) Unload driver and reload to fix quirks

 ```bash
 sudo dmesg --clear

 sudo modprobe uvcvideo -r
 sudo modprobe uvcvideo quirks=2
 ```

## 3) Plug in cam & check device is recognise (works best with SD card inserted)

 ```bash
 lsusb
    Bus 002 Device 016: ID 1b3f:0c52 Generalplus Technology Inc. 808 Camera 
    #9 (mass storage mode)
 ```


## 4) Press the "power" button on the side of the cam

 ```bash
 lsusb
    Bus 002 Device 017: ID 1b3f:2002 Generalplus Technology Inc. 808 Camera 
    #9 (web-cam mode)
 ```


## 5) Confirm video0

 ```bash
 ls /dev/video0
 ``` 

## 6) Check dmesg for errors

 ```bash
 dmesg
 ``` 

## 7) Run mplayer
 ```bash
 mplayer tv:// -tv driver=v4l2:device=/dev/video0

 # streamer je také asi třeba nainstalovat a hlavně zjistit co vlastně dělá
 # streamer -c /dev/video0 -b 16 -t 5 -r 2 -s 640x480 -o /root/eyetoy00.jpeg
 ```