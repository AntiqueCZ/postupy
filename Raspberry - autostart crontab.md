# Raspberry Pi - autostart skriptů - crontab
~~- nepodařilo se rozchodit!!!~~

## 1) Spouštěcí script

- v adresáři `/home/pi` vytvořit spustitelný bash script:

```bash
# neexistující sobor se případně vytvoří...
sudo nano /home/pi/launcher.sh
```

- soubor může obsahovat např.:

```bash
#!/bin/bash
cd /
cd home/pi/python/TCU/
python3 main.py
cd /
```

- kvůli problémům, které má crontab s přístupy k adresářům, je potřeba python skripty spouštět přes bash skripty

- před dalším postupem je dobré ověřit funkčnost souboru `launcher.sh`  



## 2) Nastavení parametrů skriptů

- souboru `/home/pi/launcher.sh` je nutno nastavi příznak spustitelnosti

```bash
# nastavení příznaku spusitelnosti
sudo chmod 755 /home/pi/launcher.sh

# a ověření funkčnosti...
cd /home/pi
sh launcher.sh
```

## 3) Nastavení tvorby logů pro debug

```bash
# vytvoření adresáře pro umístění logu
# kam bude crontab zapisovat problémy
cd /home/pi
mkdir logs
```

## 4) Nastavení crontabu

- crontab je daemon, který spuští skripty v určitý čas
- [odkaz na podrobnější popis crontabu](https://www.lifewire.com/crontab-linux-command-4095300)
- [odkaz na příklady použití](https://www.thegeekstuff.com/2009/06/15-practical-crontab-examples/)

- příkazem `sudo crontab -e` otevřít edit (asi nano) a úplně nakonec přidat řádek:

```bash
# parametr @reboot zaručí jen jedno spuštění, nikoli při každém přihlášení k RPi
@reboot sh /home/pi/launcher.sh >/home/pi/logs/cronlog 2>&1
```

- po rebootu by se měl spustit požadovaný skript, případné problémy jsou zapsány do logu