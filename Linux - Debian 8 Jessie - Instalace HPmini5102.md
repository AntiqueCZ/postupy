
# Instalace Linux Debian 8 Jessie na HPmini 5102

- instalace vyšších verzí kolabuje na různých detailech
- nutná light verze GUI, zvoleno LXDE



&nbsp;</br>&nbsp;</br>

---------------------
### 1) Příprava médií
---------------------

- stáhnout verzi Debian 8 Jessie pro 64b architekturu (AMD64)
  - [debian-8.11.0-amd64-netinst.iso](https://cdimage.debian.org/cdimage/archive/8.11.0/amd64/iso-cd/debian-8.11.0-amd64-netinst.iso)
- stáhnout ovladače WiFi b43 (//NAS/Install/_Drivers...)
- na Flash (min. 4GB) nahrát přes Rufus stažený obraz instalačního média
- na další Flash nahrát do x:/b43 soubory s ovladači pro wifi, formátovátování FAT/FAT32
  - ucode15.fw
  - lp0initvals15.fw
  - lp0bsinitvals15.fw







&nbsp;</br>&nbsp;</br>

-------------------------
### 2) Spuštění instalace
-------------------------
- nabootovat z USB flash (BootMenu > F9), není dobré bootovat z SD karty, přinášelo to problémy v průběhu pozdějších fázích instalace
- projít a postupně nastavit parametry instalace
- při zadrhnutí na ovladačích pro wifi mu na dalším USB portu podstrčit připravené ovladače na druhé flash - **nevytahovat instalační flash, může dojít k unmountu!!!**
- při výběru umístění instalace je dobré mít na HDD připravenu partition pro SWAP a pro vlastní instalaci
- je nutno mu ukázat vybranou partitionu, nastavit formát (Ext4), nastavit přeformátování (nikoli smazání, to jede bit po bitu a je to na hodiny) a nakonec přípojný bod (root = `/`)
- instalace / stahování po WiFi je možné, ale je to na cca 90 minut
- nakonec je nutno potrvdit instalaci zavaděče GRUB (je-li na HDD ještě oddíl s Win7) a ukázat mu HDD (**nikoli nevytažené USB Flash!!!**)
- z plně nabité baterie to vytáhne asi polovinu






&nbsp;</br>&nbsp;</br>

--------------------------
### 3) Nastavení prostředí
--------------------------

#### Administrátorská práva `sudo`

- pro uživatele `uzivatel` je nutné povolit práva pro příkaz `sudo`
- je nutno editovat příslušný soubor pouze editorem `visudo` pod root účtem, jiná editace běžnými nástroji není možná

- pod řádek root... přidat řádek uzivatel...
- ovládání editoru je jako v nano

```bash
# User privilege specification
root      ALL=(ALL:ALL) ALL
uzivatel  ALL=(ALL) NOPASWD: ALL
```



#### LAN

- vyskytne-li se chyba `connect: Network is unreachable` i přes jinak funkční LAN, je chyba v DHCP serveru, který špatně rozdává IP GateWay a nastavení v routovací tabulce je na 0.0.0.0

```bash
# zobrazení routovací tabulky
sudo route -n


Kernel IP routing table  
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface  
192.168.1.0     0.0.0.0         255.255.255.0   U     0      0        0 wlan0
```
- oprava spočívá v nastavení správné IP adresy GateWay serveru, které ovšem **nepřežije restart**

```bash
sudo route add default gw 192.168.1.1

Kernel IP routing table  
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface  
0.0.0.0         192.168.1.1     0.0.0.0         U     0      0        0 
192.168.1.0     0.0.0.0         255.255.255.0   U     0      0        0 wlan0
```


#### WiFi ovladače

- nejsou-li ovladače pro BCM4312 WiFi module načteny při instalaci, lze je s možnými obtížemi nainstalovat následujícím postupem:



```bash
# otevřít soubor:
sudo nano /etc/apt/sources.list

# přidat do něj řádek s non-free obsahem
deb http://httpredir.debian.org/debian stretch main contrib non-free

# uložit (CTRL+X a y) a poté aktualizovat seznamy balíčků
sudo apt-get update

# instalace ovladačů b43
sudo apt-get install firmware-b43-installer

```



#### Tray 

- vše se ovládá v panelu nastavení lišty, přes pravé myšítko `Nastavení panelu`
- nastavit Geometrie/Velikost - výška 32, Velikost ikon: 30
- nastavit Vzhled/Pozadí a Vzhled/Písmo
   - [x] Systémový motiv
   - [ ] Vlastní barva
- přidat v Aplety panelu:
  - Prostor
  - Přepínač rozvržení klávesnice
  - Prostor
  - Sledování baterie
  - Prostor
- nastavit Prostor na 6px
- nastavit formát Digitálních hodin na ` | %A | %x | %R |  `
  - [x] Tučné písmo

#### Klávesnice

- lze smysluplně nastavit jen přes Nastavení v Panelu tray
- nastavit v Advanced setxkbmap Options
  - [ ] Keep system layouts
- v rozvržení klávesnice přidat Czech QWERTY
- přepnout velikost ikon panelu na 1


#### Bluetooth myš

- lépe se na to vysrat, funkčnost problematická
- jinak se BT ovládá v konzoli, kde se příkazem `bluetoothctl` spustí _BT terminál_ 
- příkaz `help` vypíše seznam možných příkazů
- postup je plus mínus asi takovýto:

```python
power on
agent on
scan on
devices #ukáže MAC adresy nascanovaných zařízení
trusted <MAC>
pair <MAC>
connect <MAC>
```

#### Tweaks-UP

##### Zavírání víka NTB
- open the file `/etc/systemd/logind.conf` as root
  >`sudo nano /etc/systemd/logind.conf`
- find this: `HandleLidSwitch`
- if it's commented, uncomment and change the value to ignore. The line after editing should be: `HandleLidSwitch=ignore`
- Restart computer and your problem should be gone. Or better restart logind service

  >`sudo service systemd-logind restart`

##### Screen saver

- je dobré ho vypnout, žere velmi mnoho procesorového výkonu a tím i baterie!!!
- /Start Menu/Volby/Screensaver -> Mode: Disable 




&nbsp;</br>&nbsp;</br>

----------------------------
### 4) Instalace SW vybavení
----------------------------

#### No Machine - Remote desktop

- ze stránek [NoMachine](www.nomachine.com) stáhnout instalační soubor `*.deb`
- spustit instalaci
- `sudo dpkg -i nomachine_6.2.4_1_amd64.deb`
- náhrada za VNC, fungoval na první dobrou
- NoMachine nerozlišuje server / klient, aplikace je společná
- nutno hlídat přípojné IP, občas je změní dle svého a nelze se připojit na vzdálený počítač

#### WingIDE v6.0

- nutno stáhnout instalaci ze stránek [WingWare](https://wingware.com/downloads/wing-pro)
- získán klíč pro verzi Pro
- poté spustit `sudo dpkg -i wingide6_6.1.0.deb`
- vyžaduje 64-bit OS!

#### Chromium
 
- odinstalace Forefoxu: `sudo apt-get remove firefox-esr`
- instalace chromium: `sudo apt-get install chromium`

#### GIT

- `sudo apt-get install git gitk git-gui nano` 

- lze nainstalovat i gui git správce Kraken, ale strašně dlouho se na HPmini spouští, pak ale funguje celkem spolehlivě, nicméně je asi lepší využívat "_nativní_" git s gui

#### Typora MarkDown editor

- stáhnout ze stránek Typora.io zapakovaný soubor [Typora-linux-x64.tar.gz](https://typora.io/linux/Typora-linux-x64.tar.gz) s instalací
- rozbalit do adresáře `/opt/`
- vytáhnout ikonu na plochu a spustit





&nbsp;</br>&nbsp;</br>

------------------------------------------
### 5) Seznam nefunkčního SW po odzkoušení
------------------------------------------
###### Remote desktop
- TightVNC, TigerVNC
- zkoušen RealVNC, s problémy se nainstaloval a nespolehlivě rozchodil
- TeamViewer potřebuje příliš mnoho závislých balíčků, které nešlo nainstalovat

###### MarkDown Editory
- MarkDown Plus - není pro Linux
- Atom jsem vůbec nezkoušel
