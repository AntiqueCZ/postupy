# Win 10 - Reset hesla

- lze pouze pro místní uživatele, nikoli pro online a doménové

- zdroj: [Jak se dostat do Windows 10, když jsem zapomněl(a) heslo](https://wintip.cz/610-jak-se-dostat-do-windows-10-kdyz-jsem-zapomnel-a-heslo)

## 1. S přístupem do windows
Po přihlášení do windows pomocí jiného uživatele (samozřejmě s právy administrátora, uživatelský je k ničemu) spustit v cmd (opět s admin právy) konzoli pro řízení uživatelských účtů:
```dos
C:> control userpasswords2
```
Zde lze vybrat uživatele a resetovat mu jeho heslo, i bez znalosti hesla původního.

Je-li možno se do Win10 dostat přes PIN a je v nich pouze jeden uživatel, je nutné vytvořit dalšího uživatele s admin právy. Pod vlastním účtem si nelze změnit vlastní heslo. Resp. lze ale pouze se znalostí původního.


## 2. Bez možnosti přihlášení do Win10

Cílem je opět spustit konzoli pro řízení uživatelských účtů.

- ve Win10 utilitu `c:\windows\system32\utilman.exe`  (spouští Usnadnění přístupu) nahradit za cmd.exe
    - k tomu je třeba nabootovat jinak než z Win10, nebo z instalace Win10 (až se zobrazí okno s instalací Windows, stisknout klávesovou zkratku [Shift]+[F10] a spustí se příkazový řádek s právy admina -> je nutné zjistit správnou cestu k souboru, obvykle `D:\windows\system32\utilman.exe`)
  
```dos
C:> cd D:\windows\system32
D:\Windows\system32> ren utilman.exe utilman.old
D:\Windows\system32> copy cmd.exe utilman.exe
```

- poté je nutno zavřít příkazový řádek a zrušit/opustit instalaci -> restartovat počítač
- na úvodní obrazovce s polem pro zadávání hesla kliknout na ikonu `Usnadnění přístupu` -> protože je tato aplikaci nahrazena příkazovým řádkem, spustí se právě ten s právy administrátora
- stejně jako v bodě 1. spustit `control userpasswords2` a v konzoli resetovat heslo