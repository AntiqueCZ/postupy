# Python - Flask WEB framework

- popis tvorby webu pro kontrolu topení v bytě - upgrade diplomové práce
- rozšíření/úprava na terárium
- základní stavba kostry webu bude na Win10, poté translokace na Linux/Raspbian pro doladění GPIO 

Zdroje informací:  
[Flask official web](http://flask.pocoo.org)  
[IT network - Python/Flask](https://www.itnetwork.cz/python/flask/seznameni-s-flask-microframeworkem)  
[Nauč se Python - Flask](https://naucse.python.cz/lessons/intro/flask/)



## Instalace na Raspbian (RPi Zero W)
- stažení aktuálního Raspbianu Lite
- vytvoření boot SD karty
- spuštění na RPi s HDMI výstupem na monitoru
- přes příkaz `sudo raspi-config` nastavit WiFi a povolit připojení přes SSH
- přes příkaz `sudo raspi-config` nastavit lokální časovou zónu (EU/Prague)
- v SSH konzoli spustit aktualizaci Raspbianu a instalaci Flasku
- nastavit automatické spuštění po startu

```bash
# aktualizace - pozor, operace tak na 20-30 minut!
sudo apt update
sudo apt upgrade

# instalace PIP
sudo apt install python3-pip

# instalace FLASK
python3 -m pip install Flask

# instalace Samba Client
sudo apt-get install  samba-common smbclient samba-common-bin smbclient  cifs-utils

# instalace ntplib
python3 -m pip install ntplib

# instalace GPIOzero
sudo apt install python3-gpiozero

# instalace serial portu
python3 -m pip install pyserial

# instalace GIT
sudo apt install git

# zkopírování instalace TCU do RPi
cd ./home/pi
git clone "https://AntiqueCZ@gitlab.com/AntiqueCZ/TCU"

# spuštění TCU
cd ./home/pi/TCU
python3 main.py

```
### Errors
- `PermissionError: [Errno 13] Permission denied` 
    - pokus o spuštění serveru na vyhrazených portech (80,81,...) bez práva roota
    - buď skript spuštět jako root (sudo ...)
    - nebo použít vysoký/nevyhrazený port (>5000)
    

### Nastavení spuštění po startu systému (crontab)
- nejprve je nutné nastavit autologin uživatele:
    - provede se přes `sudo raspi-config`, položka 3: Boot Options -> B1: Desktop/CLI -> B2: Console Autologin

- pak vytvořit spustitelný skript ~/tcu.sh

    ```bash
    sudo nano ~/tcu.sh
    ```

- ... a vložit do něj obsah (více info v souboru `Raspberry - autostart crontab.md`):

    ```bash
    #!/bin/bash
    #tcu.sh
    #   navigate to home directory,
    #   then to this directory,
    #   then execute python script,
    #   then back home
    
    cd /
    cd home/pi/TCU
    python3 main.py &
    cd /
    ```

- nastavit skriptu příznak spustitelnosti a ověřit funkčnost

    ```bash
    # nastavení příznaku spusitelnosti
    sudo chmod 755 /home/pi/tcu.sh

    # a ověření funkčnosti...
    cd /home/pi
    sh tcu.sh
    ```

- zapsat do tabulky crontab níže uvedené řádky, po rebootu by měl webserver sám naběhnout...
    
    ```bash
    # při prvním spuštění je na výběr textový editor
    sudo crontab -e

    # vytvoření adresáře pro logování
    mkdir ~/logs
    ```

    ```
    PYTHONPATH=/home/pi/.local/lib/python3.7/site-packages
    @reboot sh /home/pi/tcu.sh >/home/pi/logs/cronlog 2>&1
    ```

## Instalace Flask WIN10
Je nutné mít nainstalován Python 3.5+ a PIP (PIP bývá již součástí Win instalace Pythonu, na linuxu je nutné nainstalovat zvlášť).  
~~Dále bude potřeba pro formuláře plugin do Flasku WTForms~~

Ve Win10 nefunguje (resp. nevím přesně jak) multithread, vícevlákno je třeba zakomentovat...

```shell
$ # sudo apt install python3-pip
c:> python -m pip install Flask

```
Pro vývoj kódu na Win je nutno adresář s projektem připojit (mount) do RPi:

```bash
# automatické připojování adreářů protokolu Samba
# nejprve vytvořit adresář, kam se bude připojovat
mkdir -p /home/pi/NAS-Repository/TCU

# zjistit uid uživatele (zde je defaultní uživatel pi), jinak je vlastníkem root a nelze pak zapisovat do souborů
id 

# do souboru /etc/fstab vložit řádek:
# //NAS/Repository/Python/TCU /home/pi/NAS-Repository/TCU cifs vers=1.0,username=pi,password=a,iocharset=utf8,sec=ntlm,uid=1000 0 0
sudo nano /etc/fstab 

# test připojení
sudo mount -a

# přepnout se do připojeného adresáře...
cd ~/NAS-Repository/TCU

# ...a spustit TCU
python3 main.py
```



## 2) Založení kostry statického webu
- v adresáři `.\TCU` vytvořit podadresáře `templates` a `static`
- všechny soubory a jejich kódování uložit v UTF-8 (with BOM)

- v souboru base.html je základ šablon pro všechny ostatní html soubory/šablony
    - definice hlaviček (kódování, link na CSS,...)

- v adresáři `static` jsou uloženy obrázky, css styly, atp.
- v rootu je soubor main.py s vlastním kódem, který spouší webserver

### Multitask
- pomocí modulu threading je kompletní webserver "zabalen" do jediné funkce a ta je spouštěna na pozadí
- v popředí jede druhá funkce zajišťující komunikaci na RS485 sběrnici

### Šablony
- v adresáři \templates jsou uloženy *.html soubory/šablony 

#### připojení šablony do souboru:
```
{% extends "sablona.html" %}
```

#### vkládání bloků kódu do šablon:
```
{% block jmeno %}
....
{% endblock %}
```
