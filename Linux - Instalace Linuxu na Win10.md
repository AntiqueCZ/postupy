# Linux pod Windows 10

### 1. Install  Windows Subsystem for Linux

1. Open PowerShell as Administrator and run:
```PowerShell
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
```
2. Restart your computer when prompted.

### 2.Install from the Microsoft Store

This section is for Windows build 16215 or later. Follow these steps to check your build. For earlier versions of Windows 10, follow these instructions using lxrun.

1. Open the Microsoft Store and choose your favorite Linux distribution.  
The following links will open the Windows store page for each distribution:  
   + Ubuntu
   + OpenSUSE
   + SLES
   + Kali Linux
   + Debian GNU/Linux

2. From the distro's page, select "Get"
 

### 3. Complete initialization of your distro
Now that your Linux distro is installed, you must initialize your new distro instance once, before it can be used.

1. Launch a distro  
To complete the initialization of your newly installed distro, launch a new instance. You can do this by clicking the "launch" button in the Windows Store app, or launching the distro from the Start menu:

    >Tip: You might want to pin your most frequently used distros to your Start menu, and/or to your taskbar!
    
    
2. The first time a newly installed distro runs, a Console window will open, and you'll be asked to wait for a minute or two for the installation to complete.

   >During this final stage of installation, the distro's files are de-compressed and stored on your PC, ready for use. This may take around a minute or more depending on the performance of your PC's storage devices. This initial installation phase is only required when a distro is clean-installed - all future launches should take less than a second.
   
   
   
   
### 3. Setting up a new Linux user account

Once installation is complete, you will be prompted to create a new user account (and its password).

This user account is for the normal non-admin user that you'll be logged-in as by default when launching a distro.

   >You can choose any username and password you wish - they have no bearing on your Windows username.

When you open a new distro instance, you won't be prompted for your password, but if you elevate a process using sudo, you will need to enter your password, so make sure you choose a password you can easily remember! See the User Support page for more info.




### 4. Update & upgrade your distro's packages
Most distros ship with an empty/minimal package catalog. We strongly recommend regularly updating your package catalog, and upgrading your installed packages using your distro's preferred package manager. On Debian/Ubuntu, you use apt:


```
sudo apt update && sudo apt upgrade
```

   >Windows does not automatically update or upgrade your Linux distro(s): This is a task that the Linux users prefer to control themselves.

You're done! Enjoy using your new Linux distro on WSL! To learn more about WSL, review the other [WSL docs](https://aka.ms/learnwsl), or the [WSL learning resources](https://aka.ms/learnwsl) page.


