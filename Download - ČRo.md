# Download mp3 souborů z webu Českého rozhlasu

- v adrese `https://dvojka.rozhlas.cz/audio-download/sites/default/files/audios/93bdb03ce1333cdc090d47f4527b300b-mp3` vyměnit hash souboru
- hash se získá po spuštění přehrávání souboru, `CTRL+U` (zobrazení kódu) a vyhledání řetězce `.mp3`