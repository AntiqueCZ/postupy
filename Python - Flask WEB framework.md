# Python - Flask WEB framework

- popis tvorby webu pro kontrolu topení v bytě - upgrade diplomové práce
- základní stavba kostry webu bude na Win10, poté traslokace na Linux/Raspbian

Zdroje informací:  
[Flask official web](http://flask.pocoo.org)  
[IT network - Python/Flask](https://www.itnetwork.cz/python/flask/seznameni-s-flask-microframeworkem)  
[Nauč se Python - Flask](https://naucse.python.cz/lessons/intro/flask/)

## 1) Instalace Flask
Je nutné mít nainstalován Python 3.5+ a PIP (PIP bývá již součástí Win instalace Pythonu, na linuxu je nutné nainstalovat zvlášť).  
Dále bude potřeba pro formuláře plugin do Flasku WTForms

```shell
$ # sudo apt install python3-pip
c:> python -m pip install Flask
c:> python -m pip install flask-wtf
```  

## 2) Založení kostry statického webu
- v adresáři `.\TCU` vytvořit podadresáře `template` a `static`
- všechny soubory a jejich kódování uložit v UTF-8 (with BOM)

- v souboru base.html je základ šablon pro všechny ostatní html soubory/šablony
    - definice hlaviček (kódování, link na CSS,...)

- v adresáři `static` jsou uloženy obrázky, css styly, atp.

- v rootu je soubor main.py s vlastním kódem, který spouší webserver

### Multitask
- pomocí modulu threading je kompletní webserver "zabalen" do jediné funkce a ta je spouštěna na pozadí
- v popředí jede druhá funkce zajišťující komunikaci na RS485 sběrnici

### Šablony
- v adresáři \templates jsou uloženy *.html soubory/šablony 

#### připojení šablony do souboru:
```
{% extends "sablona.html" %}
```

#### vkládání bloků kódu do šablon:
```
{% block jmeno %}
....
{% endblock %}
```
