# Přehled instalovaných osvědčených royšíření do VS Code

- C/C++ for Visual Studio Code (Microsoft)
- Python (Microsoft)
- Visual Studio IntelliCode (Microsoft)
- Django extension for Visual Studio Code (Baptiste Darthenay)
- Visual Studio Code CSS Support for HTML Documents (ecmel)
- Jinja for Visual Studio Code (wholroyd)
- Magic Python (MagicStack Inc.)
- Pytest IntelliSense (Cameron Maske)
- Python Indent (Kevin Rose)
- Python Path (Mathias Gesbert)
- Rainbow CSV (mechatroner)
- Todo+ (Fabio Spampinato)
