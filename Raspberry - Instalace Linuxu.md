
# Instalace Raspbian na RPi3


&nbsp;</br>&nbsp;</br>

---------------------  
### 1) Příprava médií  
---------------------

- stáhnout aktuální verzi Raspbian
  - [Raspbian Download Page](https://www.raspberrypi.org/downloads/raspbian/)
- karta µSD, min. 8GB, class 10, lépe HCI nebo XCI
- karta musí být zbavena všech svazků
- utilitou Win32DiskImager nahrát stažený obraz Raspbianu na µSD
   - na kartě se vytvoří oddíl boot, který je vidět ve Win10 a oddíl EXT4, ten vidět není



&nbsp;</br>&nbsp;</br>

-------------------------  
### 3) Instalace
-------------------------
- po vložení µSD karty do RPi a zapnutí se spustí instalace
   - login: pi
   - password: raspberry

- aktualizace seznamu balíčků a upgrade

   ```bash
   sudo apt-get update
   sudo pat-get upgrade
   ```

&nbsp;</br>&nbsp;</br>

-------------------------------------
### 3) Instalace potřebných programů
-------------------------------------

#### Administrátorská práva `sudo`

- pro uživatele `uzivatel` je nutné povolit práva pro příkaz `sudo`
- je nutno editovat příslušný soubor pouze editorem `visudo` pod root účtem, jiná editace běžnými nástroji není možná

- pod řádek root... přidat řádek uzivatel...
- ovládání editoru je jako v nano

```bash
# User privilege specification
root      ALL=(ALL:ALL) ALL
uzivatel  ALL=(ALL) NOPASSWD: ALL
```

#### SAMBA share client

 - nejprve nainstalovat základní balíčky
 - pro Raspbian Buster ještě zaktualizovat systém a cesty k repozitářům

```bash
sudo apt update
sudo apt full-upgrade
sudo reboot

sudo apt-get install  samba-common smbclient samba-common-bin smbclient  cifs-utils
```

- poté vytvořit lokální adresář a k němu připojit vzdálený sdílený adresář
- a v případě potřeby se zadáním přístupových údajů (možno vynechat)

```bash
sudo mount -t cifs //server/share /mnt/abc -o user=user,pass=password,dom=domain
```

#### Připojení vzdálených adresářů

- nejrychlejší způsob automatického připojení je přidat do souboru `/etc/fstab` (s právy admina) řádek:


`//servername/sharename  /media/windowsshare  cifs  vers=1.0,username=msusername,password=mspassword,iocharset=utf8,sec=ntlm  0  0`


- soubor `/etc/fstab` naní chráněn a přístupové údaje jsou komukoliv přístupné!!!
- řeší to vytvoření zvláštního souboru s přístupovými údaji v chráněném adresáři aktuálního uživatele:

```bash
# vytvoření souboru s přístupovými údaji
nano ~/.smbcredentials
```

- vložit do něj řádky a uložit:

`username=msusername`
`password=mspassword`

 - a změnit přístupová oprávnění k souboru:

```bash
# změna přístupových práv
chmod 600 ~/.smbcredentials
```

- a nakonec v souboru `/etc/fstab` přidat/změnit řádek 

`//servername/sharename /media/windowsshare cifs vers=1.0,credentials=/home/ubuntuusername/.smbcredentials,iocharset=utf8,sec=ntlm 0 0 `

- po uložení testnout korektní zadání:

```bash
# test připojení
sudo mount -a
```

- proběhne-li příkaz bez chyb, lze očekávat, že i po restartu bude vzdálený adreář připojen

- problém se zápisem a úpravou souborů na takto připojených adresářích je způsoben tím, že příkaz `sudo mount -a` připojí adresář a jako vlastník je root, nikoli uživatel. Je nutno zjistit id uživatele příkazem `id` do parametrů připojení připojit `uid=<id_uživatele>`, například `uid=1001`

`//servername/sharename /media/windowsshare cifs vers=1.0,credentials=/home/ubuntuusername/.smbcredentials,iocharset=utf8,sec=ntlm,uid=1001 0 0 `



#### GIT

```bash
sudo apt-get install git
```


#### Python

```bash
sudo apt-get install build-essential python3-dev
sudo apt-get install python3-gpiozero
```



#### Midnight Commander

```bash
sudo apt-get install mc
```


#### No Machine - Remote desktop

- ze stránek [NoMachine](www.nomachine.com) stáhnout instalační soubor `*.deb`
- spustit instalaci
- `sudo dpkg -i nomachine_6.2.4_1_amd64.deb`
- náhrada za VNC, fungoval na první dobrou
- NoMachine nerozlišuje server / klient, aplikace je společná
- nutno hlídat přípojné IP, občas je změní dle svého a nelze se připojit na vzdálený počítač

#### Typora MarkDown editor

- stáhnout ze stránek Typora.io zapakovaný soubor [Typora-linux-x64.tar.gz](https://typora.io/linux/Typora-linux-x64.tar.gz) s instalací
- rozbalit do adresáře `/opt/`
- vytáhnout ikonu na plochu a spustit


&nbsp;</br>&nbsp;</br>

----------------------------
### 4) Periferie RPi3
----------------------------

```bash
sudo raspi-config
```

----------------------------
### 5) Bootování bez monitoru
----------------------------

- pro provozování RPi bez připojeného monitoru (pro ovládání přes VNC) je nutno nastavit v souboru `/boot/config.txt` parametry 

```bash
# uncomment if hdmi display is not detected and composite is being output
hdmi_force_hotplug=1

# uncomment to force a specific HDMI mode (this will force VGA)
# hdmi_mode=18: 1024x768 	75Hz 	4:3
# hdmi_mode=36: 1280x1024 	75Hz 	5:4
hdmi_group=2
hdmi_mode=18
```

- `hdmi_group=1` je pro televize, `hdmi_group=2` pro monitory
- `hdmi_mode=x` nastavuje pak kombinaci pevného rozlišení a refresh, přehled všech módů je na webu [Raspberry](https://www.raspberrypi.org/documentation/configuration/config-txt/video.md)
