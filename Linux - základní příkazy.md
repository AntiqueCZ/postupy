## Linux základní příkazy

[Tutoriály pro Linux](www.systutorial.com)

### Výpisy, list

- `lsusb` - výpis usb zařízení  
- `lsblk` - výpis úložišť, vč. přípojného bodu partitionu  


- `ls -l /dev/* | wc -l` - výpis počtu připojených zařízení
- `fdisk -l` - výpis paměťových médií

### Připojení datových úložišť

- musím mít zjištěno co chci připojit
- musím mít předem vytvořený adresář kam chci připojit

```bash
mkdir /media/cokoli
mount /dev/mmcblk0p1 /media/cokoli
```  

### Připojení datových úložišť po startu

- editace souboru /etc/fstab
- 

```bash
/dev/sda3   /media/entertainment    ntfs-3g rw,auto,user,fmask=0111,dmask=0000,noatime,nodiratime   0   0
/dev/sda4   /media/other            vfat rw,auto,user,fmask=0111,dmask=0000,noatime,nodiratime   0   0
``` 

### Stahování souborů

`wget https://....` - stáhne soubor do aktuálního adresáře odkud je spuštěn