# ALT Code pro používané znaky
 
[Internetová jazyková příručka](https://prirucka.ujc.cas.cz/?id=785)


 znak | ALT code | html       
------|----------|-----
 @ | 64 
 ► | 16 
 ◄ | 17
 ▲ | 30
 ▼ | 31
 ↑ | 24
 ↓ | 25
 → | 26
 ← | 27
 ↔ | 29
 ↕ | 18
 \# | 35
 $ | 36
 % | 37
 & | 38
 ' | 39
 \* | 42
 \+ | 43
 \- | 45
 § | 0167
 × | 0215 | &times;
 • | 0149 | &bull;
 – | 0150
 … | 0133 | &hellip;
 { | 0123
 \| | 0124
 } | 0125
 ~ | 0126 | &tilde;
 mezera| 0160 | \&nbsp;
 ™ | 0153 | &trade;
 © | 0169 | &copy;
 ® | 0174 | &reg;
 ß | 0223 | &szlig;
 ± | 0177 | &plusmn;
 ÷ | 0247 | &divide;
 ° | 0176 | &deg;
 µ | 0181 | &micro;
 ü | 0252
 ö | 0246
 ä | 0228
 < | 60
 = | 61
 > | 62
 « | 174
 » | 175
 ♂ | 11
 ♀ | 12
 € | 0128 | &euro;
 ‰ | 0137 | &permil;
 
 
 # Unicode znaky nepřístupné přes ALT code
 
 Znak | Unicode | html | význam
 -----|---------|------|--------
 ∅ | U+8709 | &empty;  | průměr
 ∞ | U+8734 | &infin;  | nekonečno
 ≠ | U+8800 | &ne;     | nerovná se
 ≐ | U+8784 |          | přibližně
 ≤ | U+8804 | &le;     | 
 ≥ | U+8805 | &ge;     | 
 ∀| U+8704 | &forall; | pro všechny
 ⇒| U+8658 | &rArr;   |
 † | U+8224 | &dagger; | křížek, značka pro úmrtí
 Ω | U+8486 |          | značka pro jednotku odporu ohm
 ∑ | U+8721 | &sum;    | značka pro celkový součet
 ² | U+0178 |          | druhá mocnina 
 ³ | U+0179 |          | třetí mocnina




­