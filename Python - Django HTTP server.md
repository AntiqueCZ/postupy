# Python - Django web server - instalace a použití

[Django tutoriál - EN](https://docs.djangoproject.com/en/2.2/intro/tutorial01/)  
[Český web Django](http://www.djangoproject.cz)

## Instalace Django

Je nutné mít nainstalován Python a PIP (PIP bývá již součástí Win instalace Pythonu, na linuxu je nutné nainstalovat zvlášť)

```bash
$ sudo apt install python3-pip
$ python3 -m pip install Django
```  

<br/>

## Prvotní nastavení

 - založení projektu (skript vytvoří základní adresářovou strukturu v místě spuštění, zde adresář `pokusny_projekt`):

```bash
> python c:\64\Python\Lib\site-packages\django\bin\django-admin.py  startproject pokusny_projekt

$ ~/.local/bin/django-admin startproject pokusny_projekt
```  

### Význam jednotlivých souborů

`__init__.py` 
- prázdný soubor, který říká Pythonu, že tento adresář má být považován za Python balíček. (Pokud s Pythonem začínáte, přečtěte si více o [balíčcích v oficiální dokumentaci](http://docs.python.org/tutorial/modules.html#packages), nebo lépe v [češtině](https://naucse.python.cz/lessons/intro/distribution/))

`manage.py` 
- utilita pro příkazový řádek, která vám umožňuje projekt spravovat. Podrobnosti naleznete v [django-admin.py and manage.py](http://docs.djangoproject.com/en/dev/ref/django-admin/#ref-django-admin).

`settings.py` 
- nastavení/konfigurace pro váš Django projekt. Na stránce [Django settings](http://docs.djangoproject.com/en/dev/topics/settings/#topics-settings) se dozvíte vše podstatné.

`urls.py` 
- deklarace URL adres pro váš Django projekt; “rejstřík” vašeho webu. Podrobnosti viz [URL dispatcher](http://docs.djangoproject.com/en/dev/topics/http/urls/#topics-http-urls).


### Testovací spuštění 

- ověření, jestli výše uvedený příkaz správně zafungoval
- po spuštění serveru je na lokální adrese [127.0.0.1:8000](127.0.0.1:8000) vidět úvodní stránka úspěšné instalace

```bash
$ cd .../pokusny_projekt
$ python manage.py runserver
```  
  
- zastavení serveru je možné kombinací `CTRL+C`
- v terminálovém okně jsou průběžně vidět požadavky a odpovědi na server
- jako parametr lze zadat i IP adresu a jiný port, pak je ale nutné upravit v `settings.py` hodnotu parametru `ALLOWED_HOSTS = ['10.0.1.1']` na tuto adresu
- server si zjišťuje automaticky změny nastavení a obsahu - není třeba jej restartovat...

```bash
$ cd .../pokusny_projekt
$ python manage.py runserver 10.0.1.1:8080
```
### Vytvoření aplikace Anketa (polls)

- vytvoření souborové struktury aplikace Ankety (polls)

```bash
$ cd .../pokusny_projekt
$ python manage.py startapp polls
```
#### Vytvoření prvního náhledu stránek (view)

- v souboru `polls/view.py` zadat kód

```python
from django.http import HttpResponse

def index(request):
    return HttpResponse("Hurará, world. You're at the polls index.")
```

- ale bez url adresy pohledu to nepůjde `polls/urls.py`:

```python
from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
]
```

- a nakonec je nutné url adresy aplikace Ankety (polls) propojit s projektem (`pokusny_projekt/urls.py`): 

```python
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('polls/', include('polls.urls')),
    path('admin/', admin.site.urls),
]
```

- nyní je vytvořen pohled (view) na url adrese (index) aplikace Ankety projektu Pokus

- po spuštění serveru (`$ python manage.py runserver`) je na adrese [http://localhost:8000/polls/]( http://localhost:8000/polls/) zobrazen text "Hurará, world. You're at the polls index."

### Nastavení databáze

- upravit soubor `settings.py`
- při použití PostgreSQL nebo MySQL, musí databáze již existovat
- SQLite - databázový soubor bude automaticky vytvořen až bude třeba.

`ENGINE` 
- zvolit ‘django.db.backends.postgresql_psycopg2’, ‘django.db.backends.mysql’ nebo ‘django.db.backends.sqlite3’. Django podporuje i další databázové backendy.

`NAME` 
- jméno vaší databáze. Pokud používáte SQLite, uveďte zde absolutní cestu (včetně jména souboru) k databázovému souboru (v případě, že soubor neexistuje, bude vytvořen při první synchronizaci, viz níže). Při uvádění cesty vždy používejte dopředné lomítka (/), a to i v systému Windows (např. C:/homes/user/mysite/sqlite3.db).

`USER` 
- databázové uživatelské jméno (u SQLite se nepoužívá).

`PASSWORD` 
- databázové heslo (u SQLite se nepoužívá).

`HOST` 
- počítač, na kterém jede databáze. Pokud je na stejném počítači jako Django, nechte řetězec prázdný. (u SQLite se nepoužívá)

#### Vytvoření tabulek databází
- před prvním spuštěním je nutno spustit proces vytvoření databází, pro každou aplikaci použitou v projektu
- defaultní aplikace jsou již přednastaveny v části `# Aplication definition`

```bash
$ cd .../pokusny_projekt
$ python manage.py migrate
```

- pro prohlížení databáze SQLite lze nainstalovat:

```bash
$ sudo apt-get install sqlitebrowser
```

#### Nastavení Time Zone
- v souboru `./settings.py`

```python
LANGUAGE_CODE = 'cs-cz'
TIME_ZONE = 'Europe/Prague'
```

### Vytvoření modelu
- myšleno vytvoření databázové struktury uživatelských dat dané aplikace (zde webové anktety)

- vytvoření dvou modelů: Otázky (vlastní otázka a datum publikování) a Volba (s textem volby a vlastní hlasování)

- každá Volba je asociována s otázkou

- nastavení modelů je v souboru `/pokusny_projekt/models.py`


```python
from django.db import models


class Otazka(models.Model):
    otazka_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')


class Volba(models.Model):
    otazka = models.ForeignKey(Otazka, on_delete=models.CASCADE)
    volba_text = models.CharField(max_length=200)
    hlasovani = models.IntegerField(default=0)
```

#### Aktivování modelů

- nutno propojit aplikaci s projektem `pokusny_projekt/settigs.py` přidáním řádku do pole `INSTALLED_APPS`:

```pyhon
INSTALLED_APPS = [
    'polls.apps.PollsConfig',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
```

- vytvoření propojení:

```bash
$ cd .../pokusny_projekt
$ python manage.py makemigrations polls
```

# Ukončeno - Kanón na vrabce...