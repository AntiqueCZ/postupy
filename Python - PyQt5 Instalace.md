## PyQt5 Instalace

[GUI v Pythonu: PyQt5](https://naucse.python.cz/course/mi-pyt/intro/pyqt/)

### Instalace PyQt5

```bash
$ python -m pip install --upgrade pip
$ python -m pip install PyQt5
```  

<br/>

### Instalace QT Designeru

there is an easier way to install QT Designer without downloading GB's of data from QT, install the latest version of __pyqt5-tools__ using:

```bash
$ pip install pyqt5-tools --pre
```

The designer.exe will be installed in `...Lib\site-packages\pyqt5_tools`

<br/>

### Instalace PyQt5

```bash
$ python -m pip install numpy
```  