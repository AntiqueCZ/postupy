# Nastavení serializace ARM

&nbsp;<br />
## 1) Projekty od Beroshka

1) Instalace ovladače programátoru J-Link
    - pouze verze 6.10g, u jiných hrozí poškození programátoru a nutnost jeho opravy přeprogramováním
    
2) Do adresáře s utilitou `ARM Serializer Berosh.exe` nakopírovat z nainstalovaného ovladače J-Flash:

    - JFlash.exe
    - JLinkARM.dll
    - JLinkRDI.dll
    - \\ETC\\\*.\*
    
3) Vytvořit v `JFlash.exe` projektový soubor s definicemi procesoru