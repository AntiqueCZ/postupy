# Tvorba WWW stránek do zařízení Horatio

## Instalace

Prvotní instalace:

    1) Do µP nahrát programátorem prvotní Bootstrap.bin  
    2) V prohlížeči na IP adrese zařízení (obvykle 10.1.1.9) sedí minibootstrap, přes který je možné nahrát výkonný kód aplikace *.tea  
    3) Dále je možné bez restartu zařízení ještě nahrát zkompilovaný obraz webových stránek http.spi  
    4) Po restartu zařízení již je v prohlížeči aktuální podoba www stránek a již je možné nahrát konfiguraci zařízení  

## Generování zkompilovaného obrazu www stránek

Utilita WWW.exe má dvě funkce:  

    1) vytvořit obraz http.spi html stránek
    2) spustit simulátor www stránek pro odladění kódu

Utilita WWW.exe je ovládána konfiguračním souborem, který definuje:  

    1) !UDP=<IP adresa zařízení> - simulátor je schopen vyčítat a odesílat aktuální data/požadavky na připojené zařízení  
    2) !html=<cesta k adresáři s www stránkami>  
    3) !spi=<cesta kam má být uložen zkompilovaný obraz www stránek>  
    4) Seznam souborů zařazených do obrazu a jejich příznaků:  
       1) Jméno souboru  
       2) Příznak je-li soubor dynamický [0,1]  
       3) Požadovaná úroveň oprávnění [0=everyone, 1=user, 2=admin]  
       4) MimeType (HTML=2, GIF=3, JPEG=4, PNG=5, PDF=6, JS=7, JAR=8, CSS=9,XML=10, Plain=11, Octet/stream=12)  
       5) Příznak, má-li se soubor očistit od bílých znaků  


Po spuštění WWW.exe <config.txt> je na adrese `127.0.0.1:82` spuštěn simulátor s napojením na jednotku zadanou v proměnné !UDP.

Změny v html souborech se v prohlížeči projeví takřka okamžitě a bez nutnosti restartu zařízení či simulátoru.




## Přihlašovací údaje

login: admin  
password: xfv1bnm2

