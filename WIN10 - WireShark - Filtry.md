# WireShark - provoz na LAN síti

## Instalace
[WireShark](https://www.wireshark.org/#download)  

- defaultní instalace pro základní použití zcela postačuje

## Filtry pro zjištění konkrétního provozu

- po spuštění scanování je zobrazován veškerý provoz a pro snadnější diagnostiku je nutné vyfiltrovat jen potřebné packety

- do řádku s filtrem se zapíše daný filtr a spustí modrým tlačítkem

### Wake On LAN - WOL packety

`wol.mac`

### zdrojová IP adresa

` ip.addr==192.168.1.53`