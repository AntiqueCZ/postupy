
# OrCAD386+ Středy součástek do souboru LOC

&nbsp;</br>
-----------
## 1. Data
-----------

- získaná projektová data uložit do c:\\DOS\\ORCAD (**zachovat originál v zipu!!!**)
- případné nové knihovny uložit do c:\\DOS\\ORCADESP\\SDT\\LIBRARY

Projekt s požadovanými daty souřadnic se otevře v sekci „Design Management Tools" (odklepnutím Execute). V levé části (Design) jsou adresáře s projekty, v pravé již konkrétní soubory se schématy \*.sch. Výběrem konkrétního souboru a potvrzením `[OK]` se schéma otevře a program se vrátí do úvodního menu se sekcemi.





&nbsp;</br>
----------------------------------
## 2. Úprava dat v souboru \*.BD1
----------------------------------

- PC Board Layout Tools  
    - Modify modules > Local Configuration > Configure MODMOD_

- do `Destination` zadat nové jméno \*.BD1

- zaškrtnout:
   - [x] Specify RAM page size (4096)
       - [x] 4096
       - [ ] 2048
       - [ ] 1024
   - [x] Fix center of rotation
       - [x] Treat all modules as surfacemount

- potvrdit `OK` a `Execute` (vytvoří se upravený soubor \*.BD1)




&nbsp;</br>
------------------------------------------
## 3. Uložení souřadnic do souboru \*.LOC  
------------------------------------------

- Module Report
   - Local Configuration > Configure MODLOC_ (__nutno odklepnout Enterem, nikoli myší!!!__)

- zadat jako `Source` nově vytvořený soubor \*.BD1

- nastavit/zaškrtnout `Report all module data as text`

- kontrola exportovaných parametrů (sort x,y; dim a pos in mm)

- potvrdit `OK` a `Execute` (vytvoří se nový soubor \*.LOC)




![OrCAD](img/GOST_-_OrCAD386+_středy_součástek/image1.png)  

![OrCAD](img/GOST_-_OrCAD386+_středy_součástek/image2.png)

![OrCAD](img/GOST_-_OrCAD386+_středy_součástek/image5.png)