# GIT - Command Line 

[https://git-scm.com/book/cs/v2](https://git-scm.com/book/cs/v2)





&nbsp;</br>
## 1) Instalace

- v linuxových systémech je již obvykle obsažen v základních instalacích, proto může být instalace automaticky přeskočena / nebo přepsána novější verzí

```bash
sudo apt-get install git gitk git-gui nano

git --version  # kontrola nainstalované verze GIT
```

 
 

&nbsp;</br>
## 2) Nastavení globálních přístupových údajů

- konfigurace uživatelského jména a e-mailu pro přístup k vlastním repozitářům (úložištím)

```bash
git config --global user.name <JMÉNO_PRO_GIT>   # nastavení globálního jména
git config --global user.email <email@pro_git>  # nastavení globálního emailu
git config --global color.ui true                        # nastavení berevných výpisů v terminálu
git config --global --list                      # výpis zadaných globálních proměnných
```

- ke každému projektu lze zadat lokální user.name a user.email, potom se nezadává přepínač --global

- pro uložení přístupových údajů v linuxu lze zadat (ve Win10 je to řešeno ve Správci pověření/Credentials manager):

```bash
git config credential.helper store
git pull # řekne si o přístupové údaje
git pull # projde s uloženými údaji 

```
 


&nbsp;</br>
## 3) Nový projekt v GitLabu

- všechny operace programu git se vždy vztahují k projektu, odkud se git spouští, proto je nutno se přepnout do adresáře s projekty (zde win10):

```bash
# přepnutí do adresáře s Python projekty
$ cd a:/_CodeProjects/_Python/NovyProjekt

# nastavení streamu na GitLab a vytvoření nového projektu
$ git push --set-upstream "https://AntiqueCZ@gitlab.com/AntiqueCZ/NovyProjekt.git" master

```

### Jistější způsob:

- v GitLabu vytvořit nový projekt (lze nastavit jiné jméno a jiný adresář)
- nový projekt naklonovat kamkoli do PC (viz bod 4)
- naklonované soubory (obvykle u nového projektu jen adresář .git a Readme.md) nakopírovat do vlastního adresáře s projektovými soubory
- přidat do adresáře soubor .gitignore, který vyloučí nechtěné soubory ze sledování
- nové soubory přidat do sledování (viz bod 5)



&nbsp;</br>
## 4) Stažení projektu z GitLab repozitáře

- všechny operace programu git se vždy vztahují k projektu, odkud se git spouští, proto je nutno se přepnout do adresáře s projekty (zde Linux):

```bash
# přepnutí do adresáře s Python projekty
cd ~/PythonProjects

# stažení existujícího projektu
# projekt se stáhne/naklonuje do adresáře ~/PythonProjects/<jméno_projektu>
git clone "https://AntiqueCZ@gitlab.com/AntiqueCZ/<jméno_projektu>"

# pro další git operace s projektem je nutno se přepnout do adresáře projektu
cd ~/PythonProjects/<jméno_projektu>
```

### Selhání autentifikace ve Win10

V případě, že je špatně zadané heslo a již není možné jej změnit - není zobrazován požadavek na zadání user/pass a vše končí hláškou: remote: HTTP Basic: Access denied; fatal: Authentication failed for... je nutno přístupové údaje změnit ve Správci pověření:

`Win10_Start/Správce pověření (Credentials manager)/Přihlašovací údaje systému Windows 
položka git:https://gitlab.com`


&nbsp;</br>
## 5) Přidání souboru do projektu

- soubor je po přidání do adresáře projektu ve stavu "untracked/nesledován", jeho stav pro GIT lze ověřit výpisem:


```bash
git status          # výpis o stavu projektu
git status -s       # výpis o stavu projektu ve zkrácené verzi
```

- k zahájení sledování nových souborů se používá příkaz git add. V příkazu git add je uvedena cesta buď k souboru, nebo k adresáři. Pokud se jedná o adresář, příkaz přidá rekurzivně všechny soubory v tomto adresáři:


```bash
git add <jméno_souboru>       # změní status souboru na tracked/sledován

git add --all                 # přidá všechny nesledované soubory
```






&nbsp;</br>
## 6) Příprava změněných souborů k zapsání

- změněný soubor je ve výpisu git status uveden v části „Changes not staged for commit" (změny, které nejsou připraveny k zapsání). Znamená to, že soubor, který je sledován, byl v pracovním adresáři změněn, avšak ještě nebyl připraven k zapsání (staged). K zapsání ho připravíme provedením příkazu git add. Příkaz git add je víceúčelový - používá se k zahájení sledování nových souborů, k přípravě souborů k zapsání a k dalším věcem, jako je například označení vyřešených případů kolize souborů při slučování. Možná pomůže, když se o něm uvažuje spíše ve smyslu „_přidej tento obsah do dalšího snímku_" než ve smyslu „_přidej tento soubor do projektu_".

```bash
git diff  # zobrazení změn, které ještě nejsou přidány k zapsání (status staged)
```




&nbsp;</br>
## 7) Zapisování změn - commit

- všechno, co dosud nebylo připraveno k zapsání (všechny soubory, které byly vytvořeny nebo změněny a které nebyly přidány k zápisu příkazem git add) nebudou do revize zahrnuty

```bash
git commit  # zápis připravených změn, vytvoření snímku
```

- po zadání příkazu se otevře zvolený editor (git config --global core.editor), kde lze upravit zprávu o commitu. Po uzavření editoru Git vytvoří objekt revize se zprávou, napsanou v editoru

```bash
git commit -v  # zpráva s podrobnějším výpisem git diff
```

- zprávu lze zadat paramterem -m:

```bash
git commit -m "zpráva o commitu, popis revize nebo uskutečněných změn"
```

- přidáním parametru -a GIT do revize automaticky zahrne každý soubor, který je sledován - odpadá potřeba zadávat příkaz git add - nutná opatrnost!!!

```bash
git commit -a          # zápis všech sledovaných souborů
```



&nbsp;</br>
## 8) Odeslání commitu na server

- zapsané soubory do nové verze (commit) je nutno odeslat do cloudu na server (cesty si pamatuje z příkazu clone)

```bash
git push  # odeslání zapsaných souborů na server
```




&nbsp;</br>
## 9) Stažení / aktualizace projektu ze serveru

- změny projektu z jiných PC / jiných uživatelů se zjišťují / aktualizují příkazem git pull

```bash
git pull  # příjetí jinde změněných souborů ze serveru
```




&nbsp;</br>
## 10) GUI nadstavba

- grafické velmi nepřehledné klikátko

```bash
gitk -all
```

- grafické GUI, stejné jako na Windows 10

```bash
git gui
```


