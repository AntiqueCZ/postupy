# Konverze DOCX to MarkDown

Pomocí univerzálního CLI programu PanDoc

1. Stáhnout a rozbalit [pandoc.zip](https://github.com/jgm/pandoc/releases) pro windows x64.
2. Spustit příkazem:

```
pandoc.exe -r docx -w markdown /path/soubor.docx -o soubor.md --extract-media /path
```

> do adresáře path se uloží obrázky z wodu, nicméně je nutno cesty poupravit

>  --help vypíše seznam a vlastnosti parametrů