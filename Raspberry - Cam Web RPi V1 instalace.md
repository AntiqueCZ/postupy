# Raspberry Pi - Cam Web Install

Instalace Raspberry kamerky verze V1 jako IP kamery s vlastním AP serverem. Nejprve se nainstaluje webové rozhraní pro kameru a poté AP pro Raspberry. Vyzkoušeno pro RPi3.

- [RPi-Cam-Web-Interface](https://elinux.org/RPi-Cam-Web-Interface)
- [Setting up a Raspberry Pi as a Wireless Access Point](https://www.raspberrypi.org/documentation/configuration/wireless/access-point.md)



## 1) Instalace RPi-Cam-Web-Interface

### 1. Instalace Raspbian OS
 - na µSD kartu nahrát Raspbian Lite (Buster)
 - aktivovat SSH přístup vytvořením prázdného souboru `ssh` v rootu boot sektoru karty
 - provést aktualizaci 
     - pozor pro Buster je nutno používat příkaz `apt` nikoli `apt-get`
     - operace trvá poměrně dlouho, asi tak 15 minut

 ```bash
 sudo apt update
 sudo apt dist-upgrade
 ```

 - po upgradu se připojit přes aktivované SSH pomocí Putty (nebo z Androidu JuiceSSH)
 - změnit defaultní přístupové heslo do Raspbianu
     - nesmí se použít sudo, to se pak změní heslo pro

  ```bash
 passwd
 sudo reboot
  ```

 - nakonfigurovat Raspbian
     - **povolení RPi kamery!!!**
     - česká lokalizace prostředí
     - timezone
     
  ```bash
  sudo passwd
  sudo reboot
  ```     

 - a nakonec instalace git

  ```bash
  sudo apt install git
  ```

### 2. Instalace RPi-Cam-Web-Interface

 - stažení kopie zdrojových souborů z git repozitáře
 - spuštění instalačního souboru

  ```bash
  git clone https://github.com/silvanmelchior/RPi_Cam_Web_Interface.git
  cd RPi_Cam_Web_Interface
  ./install.sh
  ```

 - v okně pro nastavení pro adresář zvolit cam, port nechat na 80, user/pass klasika
 - nepoužívat numerickou klávesnici (!!!) - pak je nutné instalaci pustit znovu

 - po dokončení instalačních procesů (opět několik minut) a po potvrzení spuštění kamery se lze připojit na adrese `<IP of RPI>:port/<folder>`, v konkrétním případě to je `https://10.0.1.10/cam` (pro port 80)


### 3. Spuštění a nastavení WiFi Acces Pointu

- podle návodu zprovoznit AP point přístup
