# Raspberry Pi - Kodi

## 1) Pifi Digi+ 

- klon [Hifi Berry](https://support.hifiberry.com/hc/en-us/articles/205311302-Configuring-OpenElec-with-device-tree-overlays)

do souboru `config.txt` na boot oddílu µSD karty vložit řádky:

```bash
dtoverlay=hifiberry-digi
dtdebug=1
```

## 2) SupTronics X920 (& AntiqueCZ clone)

- specifikace z [RPi Wiki](http://www.raspberrypiwiki.com/index.php/X920)

do souboru `config.txt` na boot oddílu µSD karty vložit řádky:

```bash
dtparam=i2s=on
dtoverlay=iqaudio-dacplus
```

