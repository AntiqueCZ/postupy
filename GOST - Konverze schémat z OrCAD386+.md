# Konverze schémat z OrCAD386+ do formátu *.dsn

1. Zkopírovat do adresáře `v:/../schema/` soubor `*.sch` a ve stejném adresáři uložený `sdt.cfg`
2. Upravit cesty v souboru `sdt.cfg` následujícím způsobem:  
```
PDRV   = ''
PLIB   = 'V:\LIBRARY\*.LIB'
```
3. Spustit OrCAD Capture v10.5
4. Z menu vybrat `/File/Open/Design` a otevřít SDT Schematic (*.sch)
5. Do téhož adresáře uložit konvertovaný soubor `*.dsn`
6. Z menu vybrat `/Option/Schematic Page Properties...` a v záložce Grid Reference odznačit všechny zaškrtávací položky.
7. Vytisknout přes PDFCreator do pdf souboru.
