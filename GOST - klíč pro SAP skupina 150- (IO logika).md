## Klíč pro SAP - skupina 150- (IO-logika)

[TI - logic guide](http://www.ti.com/lit/sg/sdyu001ab/sdyu001ab.pdf) - popis logických obvodů, napěťové úrovně, použití,...



kód    | typ obvodu
---    | ---
0x1    | HC
0x2    | HCT
2xx    | ACT
3x1    | VHC
3x2    | VHT
4xx    | F
5x1    | LVC
5x2    | LVT
5x3    | LV


kód    | pouzdro obvodu
---    | ---
x0x    | SO
x1x    | TSSOP
x2x    | SOT23
x5x    | QFN