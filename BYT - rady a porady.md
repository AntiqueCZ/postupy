# Octomilky

Na octomilky platí i masožravky rosnatka kapská a mucholapka podivná. Osaďte jimi kuchyň a nechte práci čistě na nich. Vyzkoušet můžete i umístění rýmovníku do místa jejich výskytu.

Vůbec nejrychleji se jich ale podle vlastní zkušenosti zbavíte tak, že si do malé skleničky nalijete ocet, do něj kápnete kapku saponátu na nádobí a přidáte lžičku cukru, zamícháte a necháte na polici (ne na kuchyňské lince, tam skleničku zaručeně vylijete, někteří i několikrát denně…).


# Domácí výroba bublifuku
Rozpusťte 180 ml tekutého pracího prostředku nebo dětského šamponu v 1,8 litrech vody. Přidejte 240 ml glycerinu pro  zesílení stěny bublin. Jednoduché vyfukovátko sestrojíte tak, že jeden konec drátku zatočíte do kroužku.  


# Výroba modelovacího těsta pro malé děti
Ve velké míse smíchejte 425 g mouky, 120 ml oleje a 120 ml vody. Dobře propracujte, a pokud je třeba, přidejte vodu, aby se hmota dobře spojila. Pro pestrost můžete rozdělit těsto na několik částí, a ty pak obarvit různými odstíny potravinářské barvy.